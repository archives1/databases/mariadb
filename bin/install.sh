# install docker-compose support docker-compose yaml version 3.9
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

# install mariadb-client-core-10.1
sudo apt install -y mysql-server mariadb-client-core-10.1

