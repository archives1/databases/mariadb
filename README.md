# MariaDB


### Set environments

```bash
export MARIADB_ROOT_PASSWORD=xxx
export MARIADB_USER=xxx
export MARIADB_PASSWORD=xxx
export MARIADB_DATABASE=xxx
export MARIADB_PORT=xxx
```

### Create Dcoker-compose

```bash
envsubst < docker-compose-template.yml > docker-compose.yml
```

### Check Docker-compose format

```bash
docker-compose -f docker-compose.yml config
```

### Create image for mariadb

```bash
# Login docker
cat my_password.txt | docker login --username tokdev --password-stdin

docker build -f Dockerfile -t tokdev/mariadb:0.1.1 -t tokdev/mariadb:latest .
```

### Build and Start database via docker-compose

```bash
docker-compose build
docker-compose up -d
```

### Install mariadb server and client on Debian / Ubuntu (APT)

```bash
sudo apt install -y mariadb-server mariadb-client-10.3
```

### Test connect mariadb command line

```bash
mariadb --host localhost --port 3306 \
      --user root --password \
      --ssl-verify-server-cert \
      --ssl-ca PATH_TO_PEM_FILE
# OR
mariadb --host localhost --port 3306 \
      --user root -pxxx 

```


### How to Grant All Privileges on a Database in MariaDB

```sql
GRANT ALL PRIVILEGES ON database_name.* TO 'username'@'localhost';
CREATE USER 'user1'@localhost IDENTIFIED BY 'password1';
GRANT ALL PRIVILEGES ON *.* TO 'user1'@localhost IDENTIFIED BY 'password1';

```

**[How to Create MariaDB User and Grant Privileges](https://phoenixnap.com/kb/how-to-create-mariadb-user-grant-privileges)



