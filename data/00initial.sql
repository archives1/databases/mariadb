CREATE DATABASE IF NOT EXISTS test;
USE test;
CREATE TABLE test.contacts (
   id INT PRIMARY KEY AUTO_INCREMENT
   ,first_name VARCHAR(25)
   ,last_name VARCHAR(25)
   ,email VARCHAR(100)
) ENGINE=InnoDB;

INSERT INTO test.contacts (first_name, last_name, email)
VALUES 
	("Alexander", "Edwards", "lon.friesen@hotmail.com")
    ,("Marcia", "Brown", "kovacek.kristian@block.net")
    ,("Patricia", "Wallace", "lucious27@hotmail.com")
    ,("Taylor", "Rowe", "mariam.bechtelar@marks.biz")
    ,("Daniel", "Davis", "jedidiah.okon@klocko.com")
	,("Bonnie", "Burton", "ikling@gmail.com")
    ,("Amanda", "Middleton", "alessandra88@gmail.com")
    ,("Tammy", "Myers", "ronny49@kling.com")
    ,("Heather", "Whitaker", "zaria23@hotmail.com")
    ,("Brandon", "Lee", "zachary91@stamm.com")
    ;
	

