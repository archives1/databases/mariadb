FROM mariadb:10.9.3

# RUN apt update && apt install mysql-client-core-8.0 -y
RUN apt update && apt install mariadb-client-core-10.6 -y

ADD data/00initial.sql /docker-entrypoint-initdb.d/00initial.sql
